# Bentengan

<img src="Assets/bentengan_logo.png" width="49.5%">


## Game Information

**Description**

Bentengan is a tag team pixel game that require 2 or more player to play. It is a muliplayer game that meant to be entertaining for player. This game made in 5 months. To win in this game, you need to touch the enemy treasure. to protect your treasure, you need to capture the enemy. each player has it own power. If you're out of your treasure area, your power will slowly decrease. You can capture enemy who's power is lower than you. You can release your friend that has been captured by touching the gate. 

**General Information**

* Game Engine     : Unity 2019

* Target Platform : Desktop PC

* Rating          : 5+

* Check our [High Concept Statement](https://docs.google.com/presentation/d/17RImZHlnQ75-GBIJojP_6wiAWUMoLrpAwox9DQDekEM/edit?usp=sharing) for further info

* Reconsider check your [Game Design Document](https://docs.google.com/document/d/13JHCEPwcBQHqkuH3SGSUWJtEc_iZqW3cI01kqaJLwEU/edit?usp=sharing) for detailed info

**Game Application**

Click [Here](https://gitlab.com/Faid12/bentengan/-/blob/master/Betengan/Build/Betengan.exe) to download the game

**Credits**

[Faidurrohman](https://gitlab.com/Faid12)

[Muhammad Jatayu W.](https://gitlab.com/jatayumuw)

**Game Flow**

<img src="Assets/flowchart.png" width="49.5%">

**Client Code**

Click [Here](https://gitlab.com/Faid12/bentengan/-/tree/master/Betengan/Assets/Betengan/Scripts) 

**Server Code**

Click [Here](https://gitlab.com/Faid12/bentengan/-/tree/master/Betengan%20Server/Assets/Scripts)


## Video Documentation

<img src="Assets/Bentengan__2_.gif" width="49.5%">
